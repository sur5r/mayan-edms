from __future__ import unicode_literals

__title__ = 'Mayan EDMS'
__version__ = '3.2.4'
__build__ = 0x030204
__build_string__ = 'v3.2.4_Sat Jun 29 02:50:51 2019 -0400'
__django_version__ = '1.11'
__author__ = 'Roberto Rosario'
__author_email__ = 'roberto.rosario@mayan-edms.com'
__description__ = 'Free Open Source Electronic Document Management System'
__license__ = 'Apache 2.0'
__copyright_short__ = '2011 Roberto Rosario'
__copyright__ = '{} {}'.format('Copyright', __copyright_short__)
__website__ = 'https://www.mayan-edms.com'
